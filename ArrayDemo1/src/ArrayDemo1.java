
public class ArrayDemo1
{

	public static void main(String[] args)
	{
		int[] array1= new int[] {3,4,5};
		int[] array2= new int[] {7,1,3};
		
		addition(array1,array2);
		System.out.println();
		multi(array1,array2);

	}
	
	
	public static int addition(int[] array1, int[]array2)
	{
		System.out.print("+ ");
		int result=0;
		for (int index=0; index<array1.length; index++)
		{
			result=array1[index]+array2[index];
			System.out.print(result+",");
			
			
		}
		return result;
	}
	
	public static int multi(int[] array1, int[]array2)
	{
		System.out.print("* ");
		int result=1;
		for (int index=0; index<array1.length; index++)
		{
			result=array1[index]*array2[index];
			System.out.print(result+",");
			
			
		}
		return result;
	}
	

}
